#!/usr/bin/env python
# encoding: utf-8
"""
Read a cubit data file (*.cub) with meshed entities.

inputDataFile must be places in ../InputData/

usage:
python GetCubitData.py [-r 0/1 -p 0/1] <inputDatafile, without .cub-extension>
"""

"""
Created  by Friedhelm Cramer on 2011-11-03.
Modified by Henning Schippke on 2015-02-28.

last modification: 20.04.2017
"""


# =========================================================================== #
# module imports
# =========================================================================== #

import os, sys, time

from numpy                     import zeros, array, float64
from multiKeyDict.multiKeyDict import multi_key_dict

from CuthillMckee              import SparseMatrix

try:
    import cubit
except:
    # append python path with path to cubit located in /mnt/shared/
    sys.path.append('/mnt/shared/opt/Cubit/Cubit13.2/bin')
    try:
       import cubit
    except:
       sys.exit("!!! Process terminated !!! ... no cubit.py available ... check python path.")

# =========================================================================== #


# =========================================================================== #
# classes
# =========================================================================== #

class ControlData():
    """
    store general data needed for input and output
    """

    def __init__(self,name="Test"):
        self.name = name

        self.GetPath()


    def GetPath(self):
        self.path    = os.getcwd()
        self.inpath  = self.path + '/../Input'
        self.outpath = self.path + '/../Output'

        self.currentDate = time.strftime("%d.%m.%y")
        self.currentTime = time.strftime("%H:%M:%S")

        # multi-key-dictiondary to map dofID -> timeSlabID
        timeSlabID = multi_key_dict()
        timeSlabID[ 1, 2, 3, 4, 5, 6, 7, 8, 9,10] = 0
        timeSlabID[11,12,13,14,15,16,17,18,19,20] = 1
        timeSlabID[21,22,23,24,25,26,27,28,29,30] = 2

        self.timeSlabID = timeSlabID

# =========================================================================== #


class CubitOrdering:

    mapping = {

        # 2-node element
        'BAR'   : array( [  0, 1 ] )
        ,
        # 4-node element
        'QUAD4'     : array( [  0, 1, 2, 3] )
        ,
        # 9-node element
        'QUAD9'     : array( [  0, 1, 2, 3, 4, 5, 6, 7, 8 ] )
        ,
        # 27-node element
        'HEX27' : array( [   4,  7,  6,  5,  0,  3,  2, 1, 19 ,
                            18, 17, 16, 12, 15, 14, 13,
                            11, 10 , 9, 8,
                            22, 23, 26, 24, 25, 21, 20] )
    }

    def get(self, type):
        return self.mapping[type]

# =========================================================================== #


class MeshData():
    """
    class storing mesh data (node coordinates, connectivity, elementTable)
    """

    def __init__(self):

        self.coord        = []
        self.connect      = []
        self.elementTable = []


    def add_coord(self, nodeNumber, coord):

        coordList = coord.tolist()
        coordList.insert(0,nodeNumber)

        self.coord.append(array(coordList))


    def add_connect(self, elementNumber, connect ):

        connectivityList = connect.tolist()
        connectivityList.insert(0,elementNumber)

        self.connect.append(array(connectivityList))


    def add_elementTable(self, elementNumber, elementTypeID, matDataID, loadDataID ):

        self.elementTable.append([elementNumber, elementTypeID, matDataID, loadDataID])

# =========================================================================== #


class BCNode():
    """
    boundary condition defined for nodes like dirichlet condition
    """

    def __init__(self, node, dofIDList, value, timeID):

        self.node         = node
        self.dofIDList    = dofIDList
        self.value        = value
        self.timeID       = timeID

# =========================================================================== #


class BCElement():
    """
    boundary condition defined for elements like neumann or cauchy
    """

    def __init__(self,elem_id,direction,value,time_func,connect):

        self.elem_id    = elem_id
        self.direction  = direction
        self.value      = value
        self.time_func  = time_func
        self.connect    = connect

# =========================================================================== #


class BoundaryData():
    """
    structure to hold boundary data
    """

    velocityID = {'x' : [1, 21], 'y' : [2, 22], 'z' : [3, 23]}
    pressureID = {'x' : [4, 24], 'y' : [4, 24], 'z' : [4, 24]}
    levelSetID = {'x' : [6, 26], 'y' : [6, 26], 'z' : [6, 26]}

    mapVar2Dof = {'velocity' : velocityID, 'pressure' : pressureID, 'levelSet' : levelSetID}


    DB_ID = { 'x' : 1 , 'y' : 2 , 'z' : 3 }                        # mapping the direction for DB
    NB_ID = { 'x' : 1 , 'y' : 2 , 'z' : 3 , 'n' : 4 , 't' : 5 }    # mapping the direction for NB


    def __init__(self):

        self.DB 	  = []  # dirichlet boundary
        self.NB 	  = []  # neumann boundary
        self.CB 	  = []  # cauchy boundary

        self.IC       = []  # initial condition

        self.SSL 	       = [] # interior nodes of shear slip layer
        self.MovNodes      = [] # node numbers of moving nodes
        self.MovNodesBound = [] # node numbers of boundary nodes of moving nodes
        self.EleSSL        = [] # interior elements of shear slip layer

        self.BC 	  = {'DB'            : self.DB           ,\
                         'NB'            : self.NB           ,\
                         'CB'            : self.CB           ,\
                         'IC'            : self.IC           ,\
                         'SSL'           : self.SSL          ,\
                         'MovNodes'      : self.MovNodes     ,\
                         'MovNodesBound' : self.MovNodesBound,\
                         'EleSSL'        : self.EleSSL        }
        self.BCNode   = {'DB'            : self.DB           ,\
                         'IC'            : self.IC           ,\
                         'SSL'           : self.SSL          ,\
                         'MovNodes'      : self.MovNodes     ,\
                         'MovNodesBound' : self.MovNodesBound }
        self.BCElem   = {'NB'            : self.NB    ,\
                         'CB'            : self.CB    ,\
                         'EleSSL'        : self.EleSSL }


    def AppendBC(self, type, condition):

        self.BC[type].append(condition)

# =========================================================================== #

# =========================================================================== #
# end of classes
# =========================================================================== #


# =========================================================================== #
# functions
# =========================================================================== #

def get_value_from_expression( expression, x, y, z, value ):

    if expression == "":
		expression = "%s" % value # set homogenous value if expression is missing
    expression.replace( "x", str(x) )
    expression.replace( "y", str(y) )
    expression.replace( "z", str(z) )

    return eval( expression )

# =========================================================================== #


def SplitName(name):

    boundaryType     = name.split()[0]

    try: # nodsets DB and IC
        variableType = name.split()[1]
        direction    = name.split()[2]
        value        = name.split()[3]
        timeID       = name.split()[4]

    except: # nodset SSL (interior nodes of SSL), sidesets EleMov and EleSSL
        variableType = None
        direction    = None
        value        = None
        timeID       = 1     # Noch notwendig oder kann auch auf None gesetzt werden?

    return boundaryType, variableType, direction, value, timeID

# =========================================================================== #


def ReadCubitData( control_data ):
    """
    function to read all data from cubit file
    """

    # -- Initialise cubit --------------------------------------------------- #

    cubit.init([""])
    cubit.silent_cmd("set info off")
    cubit.silent_cmd("set echo off")
    cubit.silent_cmd("set journal off")

    # ----------------------------------------------------------------------- #


    # -- Load inputDataFile ------------------------------------------------- #

    try:
        cubit.silent_cmd( 'open "%s/%s.cub"' % (control_data.inpath, control_data.name)  )
    except:
        sys.exit("!!! Process terminated !!! ... inputDataFile not found.")

    # ----------------------------------------------------------------------- #


    # -- Initialise extractDataObjects -------------------------------------- #

    mesh_data  = MeshData()
    bound_data = BoundaryData()

    # ----------------------------------------------------------------------- #


    # -- Extract Mesh Data -------------------------------------------------- #

    print ""
    print "==============================================================="


    # -- extracting node coordinates

    print "parsing nodes    ..."
    print "                 ... reading coordinates"

    for node in cubit.parse_cubit_list( "node", "all" ):
	    mesh_data.add_coord( node, array(   cubit.get_nodal_coordinates(node) , float64 ) )

    # convert list to array
    mesh_data.coord = array(mesh_data.coord)


    # -- extracting node connectivity and element table

    print "parsing blocks   ..."

    # block 1 <2D: surface 2 | 3D: volume 2> --  added surface 2/volume 2 to block 1
    # block 1 name 'elementTypeID 4200 matDataID 1 loadDataID 1'
    # block 1 description 'string'
    # block 1 element type <QUAD4/HEX20>

    for block in cubit.get_block_id_list():

        print "                 ... reading block %s" %(block)

        name  = cubit.get_exodus_entity_name("block", block)
    	desc  = cubit.get_exodus_entity_description("block", block)
        btype = cubit.get_block_element_type(block)

        print "                     ... reading element table"

        elementTypeID = int(name.split()[1]) # hier besser als dictionary
        matDataID     = int(name.split()[3])
        loadDataID    = int(name.split()[5])

        print "                     ... reading connectivity"

        for volume in cubit.get_block_volumes(block):
            for hex in cubit.get_volume_hexes(volume):
                sys.exit("!!! warning: hex currently not supported!!!")
                # connect = array( cubit.get_expanded_connectivity("hex",hex) )
                # mesh_data.add_connect( array( connect[CubitOrdering().get( btype )] ) )
                # mesh_data.add_material( block )

        for surface in cubit.get_block_surfaces(block):
            for quad in cubit.get_surface_quads(surface):
                connect = array( cubit.get_expanded_connectivity("quad",quad) )
                mesh_data.add_connect( quad, array( connect[CubitOrdering().get( btype )] ) )
                mesh_data.add_elementTable( quad, elementTypeID, matDataID, loadDataID )

        for curve in cubit.get_block_curves(block):
            for edge in cubit.get_curve_edges(curve):
                sys.exit("test this - check BAR element type")
                # connect = array( cubit.get_expanded_connectivity("edge",edge) )
                # mesh_data.add_connect( array( connect[CubitOrdering().get( btype )] ) )
                # mesh_data.add_material( block )

    # convert list to array
    mesh_data.connect      = array(mesh_data.connect)
    mesh_data.elementTable = array(mesh_data.elementTable)

    # ----------------------------------------------------------------------- #


    # -- Extracting Boundary Data ------------------------------------------- #

    # -- extracting Dirichlet bc data from nodesets

    print "parsing nodesets ..."

    # nodeset 1 node in surface 46
    # nodeset 1 Name 'DB xyz 0.0 1'
    # nodeset 1 description ''
    # draw node in nodeset 1
    # display all

    for nodeset in cubit.get_nodeset_id_list():

        print "                 ... reading nodeset %s" %(nodeset)

        name = cubit.get_exodus_entity_name("nodeset", nodeset)
        desc = cubit.get_exodus_entity_description("nodeset", nodeset)

        # split name
        btype, varType, direction, value, timeID = SplitName( name )

        if btype == 'DB' or btype == 'IC':

            for node in cubit.get_nodeset_nodes_inclusive(nodeset):
                coord = cubit.get_nodal_coordinates(node)
                value = get_value_from_expression( desc, coord[0], coord[1], coord[2] , value )
                # besser direkte Konvertierung

                for direc in direction:
                    bound_data.AppendBC( btype, BCNode(node, bound_data.mapVar2Dof[varType][direc], value, timeID ) )

        elif btype == 'SSL' or btype == 'MovNodes' or btype == 'MovNodesBound':

            for node in cubit.get_nodeset_nodes_inclusive(nodeset):

                bound_data.AppendBC( btype, BCNode(node, None, None, None ) )

        else:
            raise TypeError("Nodeset type is not defined. Currently defined nodeset types are: DB, IC as well as SSL, MovNodes and MovNodesBound.")



    # -- extracting Neumann bc and movable nodes data from sidesets

    print "parsing sidesets ..."

    for sideset in cubit.get_sideset_id_list():
        name = cubit.get_exodus_entity_name("sideset", sideset )
        desc = cubit.get_exodus_entity_description("sideset", sideset)

        # split name
        btype, varType, direction, value, timeID = SplitName( name )

        # surface loads/movable nodes
        for surface in cubit.get_sideset_surfaces(sideset):
            for quad in cubit.get_surface_quads(surface):
                elem_id = quad
                connect = array( cubit.get_expanded_connectivity("quad",quad) )
                # calculate values of load function at each node
                # values = zeros(connect.size)

                #for i,node in enumerate(connect):
                #    coord = cubit.get_nodal_coordinates(node)
                #    values[i] = get_value_from_expression( desc, coord[0], coord[1], coord[2] , value )

                # for direc in direction:
                #     bound_data.AppendBC( btype, BCElement(elem_id, bound_data.NB_ID[direc], value, time_func, connect, [plist , values]) )

                #                           BCElement(elem_id, direction, value, time_func, connect)
                bound_data.AppendBC( btype, BCElement(elem_id, None     , None,  None     , connect) )

        # line loads
        #for curve in cubit.get_sideset_curves(sideset):
        #    for edge in cubit.get_curve_edges(curve):
        #        connect = array( cubit.get_expanded_connectivity("edge",edge) )
	    #	     set_trace()
        #        # calculate values of load function at each node
        #        values = zeros(connect.size)
        #        for i,node in enumerate(connect):
        #            coord = cubit.get_nodal_coordinates(node)
        #            values[i] = get_value_from_expression( desc, coord[0], coord[1], coord[2] , value )
        #        for direc in direction:
        #            bound_data.AppendBC( btype, NeumannCauchy(node, bound_data.NB_ID[direc], value, time_func, connect, [plist , values]) )

    # ----------------------------------------------------------------------- #


    return mesh_data, bound_data

# =========================================================================== #


def PerformRevCMK(mesh_data, bound_data, options):

    print " "
    print "performing reverse Cuthill-McKee renumbering ..."
    print "                                             ... rearranging connectivity"
    print "                                             ... rearranging coordinates"
    print "                                             ... boundary conditions"

    spm = SparseMatrix()

    # reorder connectivity
    mesh_data.connect = spm.OrderSparseMatrix( mesh_data.connect, options.plot, False )

    # reorder nodenumbers
    mesh_data.coord = spm.OrderCoord_x0( mesh_data.coord )

    # reorder bound data
    spm.OrderBoundary( bound_data )

    return mesh_data, bound_data

# =========================================================================== #


def ReadCommandLineArguments(cliArguments):

    from optparse import OptionParser

    parser = OptionParser("usage: %prog [options] BaseInputFile")

    parser.add_option('-r', action="store", dest="renumber", type="int", default=0,\
                      help="renumber nodes with reverse Cuthill-McKee algorithm")
    parser.add_option('-p', action="store", dest="plot",     type="int", default=0,\
                      help="plot connectivity matrix")

    (options, args) = parser.parse_args(cliArguments)

    if len(args) > 0:
        name = args[0]
    else:
        parser.print_help()
        sys.exit()

    return name, options

# =========================================================================== #


def ExtractCubitData(cliArguments):

    # -- read command line arguments ---------------------------------------- #

    name, options = ReadCommandLineArguments(cliArguments)

    # ----------------------------------------------------------------------- #


    # -- define control data ------------------------------------------------ #

    control_data = ControlData(name)

    # ----------------------------------------------------------------------- #

    # -- read cubit data ---------------------------------------------------- #
    # extract cubit data from *.cub file
    # function imported from module CubitData

    mesh_data, bound_data = ReadCubitData( control_data )

    # ----------------------------------------------------------------------- #


    # -- perform reverse Cuthill-McKee reordering --------------------------- #

    if options.renumber:
        mesh_data, bound_data = PerformRevCMK(mesh_data, bound_data, options)

    # ----------------------------------------------------------------------- #

    return control_data, mesh_data, bound_data

# =========================================================================== #

# =========================================================================== #
# end of functions
# =========================================================================== #


# =========================================================================== #

if __name__ == '__main__':


    # -- Extract Cubit Data  ------------------------------------------------ #

    ExtractCubitData(sys.argv[1:])

    # ----------------------------------------------------------------------- #

# =========================================================================== #
# =========================================================================== #
