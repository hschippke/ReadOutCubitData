#!/usr/bin/env python
# encoding: utf-8
"""
Converts a cubit data file (*.cub) with meshed entities to
to user input data files (*.csv).

inputDataFile must be places in ../InputData/

outputData is written to ../OutputData/

usage:
python WriteCubitData2Csv.py [-r 0/1 -p 0/1] <inputDatafile, without .cub-extension>
"""

"""
Created  by Friedhelm Cramer on 2011-11-03.
Modified by Henning Schippke on 2015-02-28.

last modification: 20.04.2017
"""


# =========================================================================== #
# module imports
# =========================================================================== #

import os, sys

from numpy          import savetxt

from GetCubitData   import ExtractCubitData

# =========================================================================== #


# =========================================================================== #
# functions
# =========================================================================== #

def WriteMeshFile(mesh_data, control_data):
    """
    write mesh data (node coordinates, connectivity and element table) to *.csv files
    """

    print " "


    # -- Write node coordinates to file ------------------------------------- #

    print "write node coordinates ..."

    file = open('%s/nodeCoordinates.csv' % ( control_data.outpath ), 'w')

    file.write("# %s -- node coordinates \n" %control_data.name)
    file.write("# data extracted from *.cub file: %s, %s\n\n" %(control_data.currentDate, control_data.currentTime) )

    file.write("# number of nodes: %s\n\n" % len(mesh_data.coord) )

    file.write("# nodeNumber, xCoord, yCoord, zCoord \n\n")

    for coord in mesh_data.coord:
        savetxt(file,  coord.reshape(1,4), fmt=['%6i', '%12.5e', '%12.5e', '%12.5e'], delimiter=', ' )

    file.close()

    # ----------------------------------------------------------------------- #


    # -- Write connectivity to file ----------------------------------------- #

    print "write connectivity     ..."

    file = open('%s/nodeConnectivity.csv' % ( control_data.outpath ), 'w')

    file.write("# %s -- node connectivity \n" %control_data.name )
    file.write("# data extracted from *.cub file: %s, %s\n\n" %(control_data.currentDate, control_data.currentTime) )

    file.write("# number of elements: %s, number of nodes: %s\n\n" % (len(mesh_data.connect), len(mesh_data.coord)) )

    file.write("# elementNumber, nodeNumbers \n\n")

    for connect in mesh_data.connect:
        savetxt(file,  connect.reshape(1,-1), fmt='%6i', delimiter=', ' )

    file.close()

    # ----------------------------------------------------------------------- #


    # -- Write element table to file ---------------------------------------- #

    print "write element table    ..."

    file = open('%s/elementTable.csv' % ( control_data.outpath ), 'w')

    file.write("# %s -- element table \n" %control_data.name)
    file.write("# data extracted from *.cub file: %s, %s\n\n" %(control_data.currentDate, control_data.currentTime) )

    file.write("# number of elements: %s\n\n" % len(mesh_data.connect) )

    file.write("# elementNumber, elementTypeID, matDataID, loadDataID \n\n")

    for elementTable in mesh_data.elementTable:
        savetxt(file,  elementTable.reshape(1,4), fmt=['%6i', '%4i', '%1i', '%1i'], delimiter=', ' )

    file.close()

    # ----------------------------------------------------------------------- #


 # ========================================================================== #


def WriteBoundFile(bound_data, control_data):
    """
    write Dirichlet boundary data to condBoundDir.csv file
    """

    if bound_data.DB or bound_data.NB:
        print " "

        timeSlabID = control_data.timeSlabID


    # -- Write Dirichlet bc data to file ------------------------------------ #

    if bound_data.DB:
        print "write Dirichlet boundary   data ..."

        switchID   = 1
        sysIndexID = 0

        DB = []
        for bound in bound_data.DB:
            DB.append("%s, %s, %s, %s, %s, %s, %s\n" % (bound.node,\
                                                        bound.dofIDList[0],\
                                                        bound.value,\
                                                        bound.timeID,\
                                                        timeSlabID[bound.dofIDList[0]],\
                                                        switchID,\
                                                        sysIndexID ))
            DB.append("%s, %s, %s, %s, %s, %s, %s\n" % (bound.node,\
                                                        bound.dofIDList[1],\
                                                        bound.value,\
                                                        bound.timeID,\
                                                        timeSlabID[bound.dofIDList[1]],\
                                                        switchID,\
                                                        sysIndexID ))

        file = open('%s/condBoundDir.csv' %( control_data.outpath ), 'w')

        file.write("# %s -- Dirichlet boundary data \n" %control_data.name)
        file.write("# data extracted from *.cub file: %s, %s\n\n" %(control_data.currentDate, control_data.currentTime) )

        file.write("# number of Dirichlet bc: %s\n\n" %(len(DB)) )

        file.write("# nodeID, dofID, value, timeID, timeSlabID, switchID, sysIndexID\n\n")

        file.writelines(DB)
        file.close()

    # ----------------------------------------------------------------------- #


    # -- Write Neumann bc data to file -------------------------------------- #

    if bound_data.NB:
        print "write Neumann boundary     data ..."
        sys.exit("!!! Process terminated !!! ... Writing of Neumann boundary data currently not supported.")

        # NB = []
        # for bound in bound_data.NB:
        #     # NB.append("%s, %s, %s, %s, %s, %s, %s\n" % (bound.node,\
        #                                                   bound.dofIDList[0],
        #                                                   bound.value,\
        #                                                   bound.timeID,\
        #                                                   timeSlabID[bound.dofIDList[0]],
        #                                                   switchID, sysIndexID ))

        # file = open('%s/condBoundNeu.csv' %( control_data.outpath ), 'w')

        # file.write("# %s -- Neumann boundary data \n" %control_data.name)
        # file.write("# data extracted from *.cub file: %s, %s\n\n" %(control_data.currentDate, control_data.currentTime) )

        # file.write("# number of Neumann bc: %s\n\n" %(len(NB)) )

        # file.write("# nodeID, dofID, value, timeID, timeSlabID, switchID, sysIndexID\n\n")

        # file.writelines(DB)
        # file.close()

    # ----------------------------------------------------------------------- #


 # ========================================================================== #


def WriteInitialConditionFile(bound_data, control_data):
    """
    write initial condition to condInitial.csv
    """

    # -- Write initial condition data to file ------------------------------- #

    if bound_data.IC:
        print " "
        print "write initial   condition  data ..."

        IC = []
        for bound in bound_data.IC:
            IC.append("%s, %s, %s\n" % (bound.node, bound.dofIDList[0], bound.value) )
            IC.append("%s, %s, %s\n" % (bound.node, bound.dofIDList[1], bound.value) )

        file = open('%s/condInitial.csv' %( control_data.outpath ), 'w')

        file.write("# %s -- initial condition data \n" %control_data.name)
        file.write("# data extracted from *.cub file: %s, %s\n\n" %(control_data.currentDate, control_data.currentTime) )

        file.write("# number of initial conditions: %s\n\n" %(len(IC)) )

        file.write("# nodeID, dofID, value\n\n")

        file.writelines(IC)
        file.close()

    # ----------------------------------------------------------------------- #


 # ========================================================================== #


def WriteMovingNodesFile(bound_data, control_data):

    if bound_data.MovNodes or bound_data.MovNodesBound or bound_data.EleSSL or bound_data.SSL:
        print " "


    # -- Write movable nodes data to file ----------------------------------- #

    if bound_data.MovNodes:
        print "write movable nodes data ..."

        # !!! veraltet !!!
        #   MV = []
        #   for bound in bound_data.MV:
        #       for ii in xrange(bound.connect.size):
        #           MV.append("\t %s\n" % bound.connect[ii] )

        # get movable node numbers
        MovNodes = []
        for bound in bound_data.MovNodes:
            MovNodes.append("%s\n" % (bound.node) )


        # write node numbers which are moving with time to file "meshMovNodes.csv"
        file = open('%s/meshMovNodes.csv' %( control_data.outpath ), 'w')

        file.write("# %s -- mesh moving data :: movable nodes \n" %control_data.name)
        file.write("# data extracted from *.cub file: %s, %s\n\n" %(control_data.currentDate, control_data.currentTime) )

        file.write("# number of movable nodes: %s\n\n" %(len(MovNodes)) )

        file.write("# nodeID\n\n")

        file.writelines(MovNodes)
        file.close()

    # ----------------------------------------------------------------------- #


    # -- Write movable boundary nodes data to file -------------------------- #

    if bound_data.MovNodes:
        print "write movable boundary nodes data ..."

        # get movable node numbers
        MovNodesBound = []
        for bound in bound_data.MovNodesBound:
            MovNodesBound.append("%s\n" % (bound.node) )


        # write node numbers which are moving with time to file "meshMovNodes.csv"
        file = open('%s/meshMovNodesBound.csv' %( control_data.outpath ), 'w')

        file.write("# %s -- mesh moving data :: movable boundary nodes \n" %control_data.name)
        file.write("# data extracted from *.cub file: %s, %s\n\n" %(control_data.currentDate, control_data.currentTime) )

        file.write("# number of movable boundary nodes: %s\n\n" %(len(MovNodesBound)) )

        file.write("# nodeID\n\n")

        file.writelines(MovNodesBound)
        file.close()

    # ----------------------------------------------------------------------- #


    # -- Write shear slip layer data to file -------------------------------- #

    if bound_data.EleSSL:
        print "write shear slip layer data - element numbers, connectivity ..."

        EleSSL   = []
        nEleSSL  = 0
        nodesSSL = []

        for bound in bound_data.EleSSL:
            nEleSSL += 1
            EleSSL.append("%s" % (bound.elem_id) )
            for ii in xrange(bound.connect.size):
                EleSSL.append(",\t%s" % bound.connect[ii])
                nodesSSL.append(bound.connect[ii])
            EleSSL.append("\n")

        nodesSSL  = list(set(nodesSSL)) # make list of node numbers unique
        nNodesSSl = len(nodesSSL)

        file = open('%s/meshMovSSL.csv' %( control_data.outpath ), 'w')

        file.write("# %s -- mesh moving data :: elements SSL \n" %control_data.name)
        file.write("# data extracted from *.cub file: %s, %s\n\n" %(control_data.currentDate, control_data.currentTime) )

        file.write("# number of elements: %s, number of nodes: %s\n\n" % (nEleSSL, nNodesSSl) )
        #file.write("# number of elements: %s\n\n" % ( nEleSSL ) )

        file.write("# elementNumber, nodeNumbers \n\n")

        file.writelines(EleSSL)
        file.close()


    # ----------------------------------------------------------------------- #


    # -- Write interior sheal slip layer nodes data to file ----------------- #

    if bound_data.SSL:
        print "write shear slip layer data - interior node numbers ..."

        SSL = []
        for bound in bound_data.SSL:
            SSL.append("%s\n" % (bound.node) )


        file = open('%s/meshMovSSLNodesInt.csv' %( control_data.outpath ), 'w')

        file.write("# %s -- mesh moving data :: interior nodes SSL \n" %control_data.name)
        file.write("# data extracted from *.cub file: %s, %s\n\n" %(control_data.currentDate, control_data.currentTime) )

        file.write("# number of interior nodes in SSL: %s\n\n" %(len(SSL)) )

        file.write("# nodeID\n\n")

        file.writelines(SSL)
        file.close()



    # ----------------------------------------------------------------------- #


    print "==============================================================="
    print " "

# =========================================================================== #


def CleanUp():

    os.system("rm ./*.pyc")
    os.system("rm ./*.jou")
    os.system("chmod -R 755 ./*.*")

# =========================================================================== #


# =========================================================================== #
# end of functions
# =========================================================================== #


if __name__ == '__main__':


    # -- Extract Cubit Data  ------------------------------------------------ #

    control_data, mesh_data, bound_data = ExtractCubitData(sys.argv[1:])

    # ----------------------------------------------------------------------- #


    # -- write mesh files --------------------------------------------------- #

    WriteMeshFile(mesh_data, control_data )

    # ----------------------------------------------------------------------- #


    # -- write boundary files ----------------------------------------------- #

    WriteBoundFile(bound_data, control_data)

    # ----------------------------------------------------------------------- #

    # -- write initial condition file --------------------------------------- #

    WriteInitialConditionFile(bound_data, control_data)

    # ----------------------------------------------------------------------- #


    # -- write moving nodes files ------------------------------------------- #

    WriteMovingNodesFile(bound_data, control_data)

    # ----------------------------------------------------------------------- #


    # -- Clean up ----------------------------------------------------------- #

    CleanUp()

    # ----------------------------------------------------------------------- #


# =========================================================================== #
# =========================================================================== #
