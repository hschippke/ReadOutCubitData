'''
Created on 10.01.2011

@author: cramer
'''
from numpy import zeros, zeros_like, array
from scipy import sparse
try:
    import pylab
except:
    print "no matplotlib"
try:
    rcm_loaded = True
    from rcm.rcm import rcm 
except:
    rcm_loaded = False
    print 'Cuthill-Mckee algorithm not available'

class SparseMatrix():
    """
    ordering sparse matrix 
    """
    def __init__(self):
        self.vector_opt_reverse = array([])
        
    def PrintMessage(self, inci):
        """ print message """
        print '---------------------------------------------'
        print '-- reverse cuthill-mckee alogrithm        ---'
        print '---------------------------------------------'
        print 'number of elements:        ', inci.shape[0]
        print 'number of nodes:           ', inci.max() + 1 - inci.min()
                    

        
    def Bandwidth(self,inci):
        """ return bandwidth of given incidence matrix """
        band = zeros(inci.shape[0])
        
        for i,connect in enumerate(inci):
            band[i] = connect.max() - connect.min() + 1
            
        return int(band.max())
            
        
    def OrderSparseMatrix(self,inci,plot=None, csr_type=False ):
        """
        minimize band width for given incidence
        
        return new incidence
        """
        if rcm_loaded:
		# print output
		self.PrintMessage(inci)
		
		# maximum node number of the incidence
		max_node = inci.max() 
		min_node = inci.min()
		
		# check if node counter starts with '0'
		if min_node == 0:
		    num_unknowns = max_node + 1
		    sub = 0
		else: 
		    sub = 1
		    num_unknowns = max_node 
		    
		if not csr_type:
		    # sparse matrix to set connectivity 
		    inci_mat = sparse.lil_matrix((num_unknowns,num_unknowns))
		    
		    # insert incidence
		    for elem in xrange(inci.shape[0]):
			for row in xrange(inci.shape[1]):
			    for col in xrange(inci.shape[1]):
				inci_mat[inci[elem][row] - sub,inci[elem][col] - sub] = 1
			
		    # set node number on diagonal
		    for i in xrange(num_unknowns):
			inci_mat[i,i] = i + 1
	    
		    # change matrix format to csr
		    inci_mat = inci_mat.tocsr()
		
		else:
		    ntriplets_inci = inci.shape[1]**2 * inci.shape[0];
		    # sparse
		    I_inci = zeros(ntriplets_inci,'i');
		    J_inci = zeros(ntriplets_inci,'i');
		    X_inci = zeros(ntriplets_inci,'i');
		    ntriplets_inci = 0;
		    
		    diagonal  = zeros(num_unknowns,'i');
		    
		    # reading of incidence
		    for elem in xrange(inci.shape[0]):
			for row in xrange(inci.shape[1]):
			    for col in xrange(inci.shape[1]):
				I_inci[ntriplets_inci] = inci[elem][row] - sub
				J_inci[ntriplets_inci] = inci[elem][row] - sub
				X_inci[ntriplets_inci] = 1;  
				ntriplets_inci = ntriplets_inci+1;

		    # assample csr matrix
		    inci_mat = sparse.csr_matrix( (X_inci,(I_inci,J_inci)), shape=(num_unknowns,num_unknowns) )
		
		#inci_mat_copy = inci_mat.copy()    
		# Cuthill-Mckee algorithm from pyamg - don't work
		#inci_mat_opt = symmetric_rcm(inci_mat_copy)
		
		# new variant - this works
		vector_opt = rcm(inci_mat)
		
		self.vector_opt = vector_opt

		# reverse vector - to transfer the data
		self.vector_opt_reverse = zeros(num_unknowns)
		for i in xrange(num_unknowns):
		    self.vector_opt_reverse[vector_opt[i]] = i

		#---------------------------------------------------
		# new incidence
		#---------------------------------------------------
		inci_new = zeros_like(inci)
	    
		for i in xrange(inci.shape[0]):
		    for j in xrange(inci.shape[1]):
			inci_new[i][j] = self.vector_opt_reverse[ inci[i][j] - sub ] + 1 

		#---------------------------------------------------
		# compute bandwidth
		#---------------------------------------------------
		print 'old bandwidth:             ', self.Bandwidth(inci)
		print 'new bandwidth:             ', self.Bandwidth(inci_new)
		print '---------------------------------------------'
		
		#---------------------------------------------------
		# plot old and new incidence
		#---------------------------------------------------
		if plot:
		    if not csr_type:
			# sparse matrix to set connectivity 
			inci_new_mat = sparse.lil_matrix((num_unknowns,num_unknowns))
			
			# insert incidence
			for elem in xrange(inci_new.shape[0]):
			    for row in xrange(inci_new.shape[1]):
				for col in xrange(inci_new.shape[1]):
				    inci_new_mat[inci_new[elem][row] - sub,inci_new[elem][col] - sub] = 1
		
			# change matrix format to csr
			inci_new_mat = inci_new_mat.tocsr()
					    
		    # markersize
		    msize = 100/num_unknowns 
		    if msize < 0.5 :
			msize = 0.5     
		    
		    # try the visualizations
		    pylab.figure()
		    pylab.subplot(121)
		    pylab.title('before Cuthill-Mckee')
		    pylab.spy(inci_mat,marker='.',markersize=msize)
		    pylab.subplot(122)
		    pylab.title('after Cuthill-Mckee')
		    pylab.spy(inci_new_mat,marker='.',markersize=msize)
		    pylab.show()
                return inci_new
        else:
		print '--- no Cuthill-Mckee algorithm available' 
                return inci
                
    
    def OrderCoord_x0(self, coord_x0):
        """
        new ordering of node coordinates
        """                        
        if self.vector_opt_reverse.shape[0] > 0:
            new_coord = zeros_like(coord_x0)    
            for i in xrange(coord_x0.shape[0]):
                new_coord[self.vector_opt_reverse[i]]  = coord_x0[i]
            
            return new_coord    
        else:
            sys.exit('no vector_opt defined - first order the sparse matrix')
                                                                            
    def OrderBoundary(self, bound_data):
        """
        new nodes for boundary
        """                        
        if self.vector_opt_reverse.shape[0] > 0:
	    # loop over boundary conditions defined for nodes 
            #BCNode = {'DB' : self.DB, 'SSLI' : self.SSLI}
	    for NodeConditions in bound_data.BCNode.itervalues():	 	
            	for cond in NodeConditions:
                    cond.node = int(self.vector_opt_reverse[cond.node-1] + 1)  
            #BCElem = {'NB' : self.NB, 'CB' : self.CB, 'SSL' : self.SSL}
	    for ElemConditions in bound_data.BCElem.itervalues():	 	
            	for cond in ElemConditions:
		    for ii in xrange(cond.connect.size):
                        cond.connect[ii] = int(self.vector_opt_reverse[cond.connect[ii]-1] + 1)  
                
        else:
            sys.exit('no vector_opt defined - first order the sparse matrix')
                                                                            
                               

if __name__ == "__main__":
    
    import sys
  
    spm = SparseMatrix()

    num_elems = 6 # 12
    
    num_node = 4
    
    #coord_x0 = zeros((21,2))
    coord_x0 = zeros((14,2))
    
    # get coord
    for y in xrange(2):
        for x in xrange(7):
            coord_x0[y*7+x][0] = x
            coord_x0[y*7+x][1] = y
    
    
    # incidence
    inci = zeros((num_elems,num_node),'i')
    
    for i in xrange(num_elems):
        inci[i] =  array([i+1,i+2,9+i,8+i])                    
    #inci = array([[1,2,5,4],[2,3,6,5]])
    
    inci_new = spm.OrderSparseMatrix(inci,True)
    
    #coord_x0 = spm.OrderCoord_x0(coord_x0)
    
